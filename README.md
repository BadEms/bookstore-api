# Bookstore API

## Özet

Basit bir şekilde Kitap mağaza yönetimine yardımcı olmak için geliştirilmiştir.
3 adat kullanıcı rolü bulunmaktadır.
Sorularınız için **mguzelsever35@gmail.com** adresine iletişime geçebilirsiniz.

## DB Mimarisi

![bookstore-ER](er.png?raw=true)

## Kurulum

Servisi çalıştırmak için geliştirme ortamınızda [NodeJS](https://nodejs.org/en/download/current) kurulu olması gerekmektedir.
Şuanki aşamasında bağımlılığı PostgreSQL'dir.
API Collection ve Env proje dizinine eklenmiştir.

### Konfigurasyon için
.env.example dosyasına göre kendi postgreSQL bilgilerini ve diğer bilgileri değiştirip .env dosyası yaratmanız gerekmektedir.

### Servis için

```bash
yarn install
```

```bash
npm run start:dev
```

## Geliştiren

Mehmet Guzelsever
