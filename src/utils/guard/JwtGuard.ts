import { Injectable, ExecutionContext } from '@nestjs/common';
import { AuthGuard as PassportGuard } from '@nestjs/passport';
import { Reflector } from '@nestjs/core';
import { User } from '@db/entities/user';
import { UserRole } from '@db/enums/userRole';
import { GenericHttpException } from '../error';
import { NotAdminRole, NotManagerRole, NotRole } from '../error/types';

@Injectable()
export class JwtGuard extends PassportGuard('jwt') {
  constructor(private reflector: Reflector) {
    super();
  }
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    await super.canActivate(context);
    const user = request.user as User;

    // user role
    const isUser = this.reflector.get<boolean>('isUser', context.getHandler());
    if (isUser) {
      if (user.role == UserRole.NONE) {
        throw new GenericHttpException(NotRole);
      }
    }

    // admin role
    const isAdmin = this.reflector.get<boolean>(
      'isAdmin',
      context.getHandler(),
    );
    if (isAdmin) {
      if (user.role != UserRole.ADMIN) {
        throw new GenericHttpException(NotAdminRole);
      }
    }

    // store-manager role
    const isManager = this.reflector.get<boolean>(
      'isManager',
      context.getHandler(),
    );
    if (isManager) {
      if (user.role != UserRole.STORE_MANAGER) {
        // admin level is high??
        if (user.role != UserRole.ADMIN) {
          throw new GenericHttpException(NotManagerRole);
        }
      }
    }

    return user.id ? true : false;
  }
}
