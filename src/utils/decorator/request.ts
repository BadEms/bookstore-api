import { User } from '@db/entities/user';
import {
  CustomDecorator,
  ExecutionContext,
  SetMetadata,
  createParamDecorator,
} from '@nestjs/common';

export const GetUser = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): User => {
    const request = ctx.switchToHttp().getRequest();
    const user = request.user;
    return user;
  },
);

export const UserRequired = (isUser: boolean): CustomDecorator<string> =>
  SetMetadata('isUser', isUser);

export const AdminRequired = (isAdmin: boolean): CustomDecorator<string> =>
  SetMetadata('isAdmin', isAdmin);

export const ManagerRequired = (isManager: boolean): CustomDecorator<string> =>
  SetMetadata('isManager', isManager);
