import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { JsonWebTokenError } from 'jsonwebtoken';
import { User } from '@db/entities/user';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_SECRET || 'JWT_SECRET',
    });
  }

  async validate(payload: Record<string, any>): Promise<User> {
    const user = await this.userRepository.findOneBy({ id: payload.id });

    if (!user) throw new JsonWebTokenError('Invalid JWT');

    return user;
  }
}
