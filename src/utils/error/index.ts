import { HttpException, HttpStatus } from '@nestjs/common';
import { HttpErrorBody } from './types';

export class GenericHttpException extends HttpException {
  constructor(
    type: HttpErrorBody,
    statusCode: number = HttpStatus.BAD_REQUEST,
  ) {
    super(HttpException.createBody(type), statusCode);
  }
}
