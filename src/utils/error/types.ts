export interface HttpErrorBody {
  errorCode: number;
  message: string;
}

export const NotRole: HttpErrorBody = {
  errorCode: 10101,
  message: 'Role process is not completed.',
};

export const NotAdminRole: HttpErrorBody = {
  errorCode: 10102,
  message: 'Admin level required.',
};

export const NotManagerRole: HttpErrorBody = {
  errorCode: 10103,
  message: 'Manager level required.',
};

export const SystemError: HttpErrorBody = {
  errorCode: 10104,
  message: 'System is busy.',
};

export const InsufficienQuantitytError: HttpErrorBody = {
  errorCode: 10105,
  message: 'Insufficient quantity.',
};

export const LoginFailed: HttpErrorBody = {
  errorCode: 10106,
  message: 'Please check your data.',
};

export const UserAlreadyExist: HttpErrorBody = {
  errorCode: 10107,
  message: 'User already exist.',
};

export const StoreAlreadyExist: HttpErrorBody = {
  errorCode: 10108,
  message: 'Store already exist.',
};

export const BookAlreadyExist: HttpErrorBody = {
  errorCode: 10109,
  message: 'Book already exist.',
};

export const UserNotFound: HttpErrorBody = {
  errorCode: 10110,
  message: 'User not found.',
};

export const AdminCannotChange: HttpErrorBody = {
  errorCode: 10111,
  message: 'Admin cannot be changed.',
};

export const StoreNotFound: HttpErrorBody = {
  errorCode: 10112,
  message: 'Store not found.',
};

export const AlreadyManager: HttpErrorBody = {
  errorCode: 10113,
  message: 'Already manager role.',
};

export const MissingStoreManager: HttpErrorBody = {
  errorCode: 10114,
  message: 'Manager and Store authorization could not be found.',
};

export const BookNotFound: HttpErrorBody = {
  errorCode: 10115,
  message: 'Book not found.',
};

export const MissingData: HttpErrorBody = {
  errorCode: 10116,
  message: 'Please check the book and store information.',
};
