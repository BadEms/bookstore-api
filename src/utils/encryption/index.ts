import * as bcrypt from 'bcryptjs';

export const encrypt = async (fullText: string): Promise<string> => {
  const salt = await bcrypt.genSalt();
  return await bcrypt.hash(fullText, salt);
};

export const compareSync = async (
  fullText: string,
  hashString: string,
): Promise<boolean> => {
  return bcrypt.compareSync(fullText, hashString);
};
