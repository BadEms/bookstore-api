export enum UserRole {
  'NONE' = 'NONE',
  'USER' = 'USER',
  'STORE_MANAGER' = 'STORE_MANAGER',
  'ADMIN' = 'ADMIN',
}
