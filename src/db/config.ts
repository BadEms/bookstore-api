/* eslint-disable @typescript-eslint/no-var-requires */
require('dotenv').config();

const NODE_ENV = process.env.NODE_ENV || 'development';

const pathConfig = {
  entities: [`${__dirname}/entities/*.{ts,js}`],
  migrations: [`${__dirname}/migrations/*.{ts,js}`],
  subscribers: [`${__dirname}/subscribers/*.{ts,js}`],
};

const defaultConfig = {
  type: 'postgres',
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  port: 5432,
  ...pathConfig,
};

const dbConfig = {
  development: {
    ...defaultConfig,
  },
};

export default dbConfig[NODE_ENV];
