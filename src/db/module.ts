import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import dbConfig from './config';
import { User } from './entities/user';
import { Book } from './entities/book';
import { Stock } from './entities/stock';
import { Store } from './entities/store';
import { StoreManager } from './entities/storeManager';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: dbConfig.type,
      host: dbConfig.host,
      database: dbConfig.database,
      port: dbConfig.port,
      username: dbConfig.username,
      password: dbConfig.password,
      logging: false,
      synchronize: true,
      keepConnectionAlive: true,
      entities: [User, Book, Stock, Store, StoreManager],
    }),
  ],
})
export class DatabaseModule {}
