import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BaseEntity } from './base';
import { IStore } from '@db/interfaces/store';
import { StoreManager } from './storeManager';
import { Stock } from './stock';

@Entity()
export class Store extends BaseEntity implements IStore {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ unique: true, nullable: false })
  name: string;

  @Column({ nullable: false })
  address: string;

  @OneToMany(() => StoreManager, (storeManager) => storeManager.store)
  storeManagers: StoreManager[];

  @OneToMany(() => Stock, (stock) => stock.store)
  @JoinColumn({ referencedColumnName: 'stock_storeId' })
  bookstores: Stock[];
}
