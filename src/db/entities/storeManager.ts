import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { IStoreManager } from '@db/interfaces/storeManager';
import { Store } from './store';
import { User } from './user';

@Entity()
export class StoreManager implements IStoreManager {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ nullable: true })
  storeId: number;

  @Column({ nullable: true })
  userId: string;

  @ManyToOne(() => Store, (store) => store.storeManagers)
  store: Store;

  @OneToOne(() => User)
  @JoinColumn()
  user: User;
}
