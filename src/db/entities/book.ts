import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { BaseEntity } from './base';
import { IBook } from '@db/interfaces/book';
import { Stock } from './stock';

@Entity()
export class Book extends BaseEntity implements IBook {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ unique: true, nullable: false })
  name: string;

  @OneToMany(() => Stock, (stock) => stock.book)
  bookstores: Stock[];
}
