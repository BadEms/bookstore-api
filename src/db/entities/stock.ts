import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Store } from './store';
import { Book } from './book';

@Entity()
export class Stock {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  storeId: number;

  @Column()
  bookId: number;

  @Column()
  quantity: number;

  @ManyToOne(() => Store, (store) => store.bookstores)
  store: Store;

  @ManyToOne(() => Book, (book) => book.bookstores)
  book: Book;
}
