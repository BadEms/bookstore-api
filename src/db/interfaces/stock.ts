export interface IStock {
  id: number;
  bookId: number;
  storeId: number;
  quantity: number;
}
