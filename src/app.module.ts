import { DatabaseModule } from '@db/module';
import { Module } from '@nestjs/common';
import { AdminModule } from './modules/admin/admin.module';
import { CommonModule } from './modules/common/common.module';
import { ManagerModule } from './modules/manager/manager.module';
import { UserModule } from './modules/user/user.module';

@Module({
  imports: [
    DatabaseModule,
    CommonModule,
    AdminModule,
    ManagerModule,
    UserModule,
  ],
})
export class AppModule {}
