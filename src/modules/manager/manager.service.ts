import { User } from '@db/entities/user';
import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Book } from '@db/entities/book';
import { Stock } from '@db/entities/stock';

import { StoreManager } from '@db/entities/storeManager';
import { requestManageBook } from './manager.dto';
import { GenericHttpException } from 'src/utils/error';
import {
  BookNotFound,
  InsufficienQuantitytError,
  MissingStoreManager,
} from 'src/utils/error/types';
import { ProcessType } from 'src/utils/enum';
import { IManageResponse } from './manager.interface';

@Injectable()
export class ManagerService {
  constructor(
    @InjectRepository(Book)
    private readonly bookRepository: Repository<Book>,
    @InjectRepository(Stock)
    private readonly stockRepository: Repository<Stock>,
    @InjectRepository(StoreManager)
    private readonly managerRepository: Repository<StoreManager>,
  ) {}

  /**
   * manage a book on own store
   * @param dto
   * @param manager
   * @returns
   */
  async manageBookByManager(
    dto: requestManageBook,
    manager: User,
  ): Promise<IManageResponse> {
    try {
      const storeManager = await this.managerRepository.findOne({
        where: { userId: manager.id },
      });
      if (!storeManager)
        throw new GenericHttpException(
          MissingStoreManager,
          HttpStatus.BAD_REQUEST,
        );
      const book = await this.bookRepository.findOne({
        where: { id: dto.bookId },
      });
      if (!book)
        throw new GenericHttpException(BookNotFound, HttpStatus.BAD_REQUEST);
      const stock = await this.stockRepository.findOne({
        where: { storeId: storeManager.storeId, bookId: book.id },
      });
      if (stock) {
        if (dto.type == ProcessType.REMOVE) {
          if (stock.quantity < dto.quantity) {
            return {
              success: false,
              quantity: stock.quantity,
              message: InsufficienQuantitytError.message,
            };
          }
          stock.quantity = stock.quantity - dto.quantity;
          await this.stockRepository.save(stock);
          return {
            success: true,
            quantity: stock.quantity,
          };
        } else {
          stock.quantity = stock.quantity + dto.quantity;
          await this.stockRepository.save(stock);
          return {
            success: true,
            quantity: stock.quantity,
          };
        }
      } else {
        if (dto.type == ProcessType.ADD) {
          const newStock = new Stock();
          newStock.bookId = book.id;
          newStock.storeId = storeManager.storeId;
          newStock.quantity = dto.quantity;
          await this.stockRepository.save(newStock);
          return {
            success: true,
            quantity: newStock.quantity,
          };
        } else {
          return {
            success: false,
            quantity: 0,
            message: InsufficienQuantitytError.message,
          };
        }
      }
    } catch (error) {
      console.log({ error });
      throw error;
    }
  }
}
