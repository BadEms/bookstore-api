import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { GetUser, ManagerRequired } from 'src/utils/decorator/request';
import { JwtGuard } from 'src/utils/guard/JwtGuard';
import { ManagerService } from './manager.service';
import { requestManageBook } from './manager.dto';
import { User } from '@db/entities/user';

@Controller('manager')
export class ManagerController {
  constructor(private readonly managerService: ManagerService) {}

  @Post('manage/book')
  @UseGuards(JwtGuard)
  @ManagerRequired(true)
  @HttpCode(HttpStatus.OK)
  async manageBook(@Body() dto: requestManageBook, @GetUser() manager: User) {
    return this.managerService.manageBookByManager(dto, manager);
  }
}
