import { DatabaseModule } from '@db/module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Book } from '@db/entities/book';
import { Stock } from '@db/entities/stock';
import { StoreManager } from '@db/entities/storeManager';
import { ManagerController } from './manager.controller';
import { ManagerService } from './manager.service';

@Module({
  imports: [
    DatabaseModule,
    TypeOrmModule.forFeature([Book, Stock, StoreManager]),
  ],
  controllers: [ManagerController],
  providers: [ManagerService],
  exports: [ManagerService, TypeOrmModule],
})
export class ManagerModule {}
