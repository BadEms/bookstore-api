import { IsEnum, IsNotEmpty, IsNumber } from 'class-validator';
import { ProcessType } from 'src/utils/enum';

export class requestManageBook {
  @IsNotEmpty()
  @IsNumber()
  bookId: number;

  @IsNotEmpty()
  @IsNumber()
  quantity: number;

  @IsNotEmpty()
  @IsEnum(ProcessType)
  type: string;
}
