export interface IManageResponse {
  success: boolean;
  quantity: number;
  message?: string;
}
