import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
} from '@nestjs/common';
import { newUser } from '../admin/admin.dto';
import { CommonService } from './common.service';

@Controller('common')
export class CommonController {
  constructor(private readonly commonService: CommonService) {}

  @Post('init')
  @HttpCode(HttpStatus.CREATED)
  async createAdmin(@Body() dto: newUser) {
    await this.commonService.createAdmin(dto);
  }

  @Get('login')
  @HttpCode(HttpStatus.OK)
  async login(@Body() dto: newUser) {
    return this.commonService.login(dto);
  }
}
