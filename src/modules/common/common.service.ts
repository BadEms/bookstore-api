import { User } from '@db/entities/user';
import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { GenericHttpException } from 'src/utils/error';
import {
  LoginFailed,
  SystemError,
  UserAlreadyExist,
} from 'src/utils/error/types';
import { compareSync, encrypt } from 'src/utils/encryption';
import { UserRole } from '@db/enums/userRole';
import { sign } from 'jsonwebtoken';
import { newUser } from '../admin/admin.dto';

@Injectable()
export class CommonService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  /**
   * add a Admin user for flow
   * @param user
   */
  async createAdmin(user: newUser) {
    try {
      const existUser = await this.userRepository.findOne({
        where: { email: user.email },
      });
      if (existUser)
        throw new GenericHttpException(
          UserAlreadyExist,
          HttpStatus.BAD_REQUEST,
        );
      const newUser = new User();
      newUser.email = user.email;
      newUser.password = await encrypt(user.password);
      newUser.role = UserRole.ADMIN;
      await this.userRepository.save(newUser);
    } catch (error) {
      console.log({ error });
      throw new GenericHttpException(
        SystemError,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  /**
   * Login for authentication and authorization
   * @param dto
   * @returns
   */
  async login(dto: newUser) {
    try {
      const user = await this.userRepository.findOne({
        where: { email: dto.email },
      });
      if (!user)
        throw new GenericHttpException(LoginFailed, HttpStatus.BAD_REQUEST);
      const checkPass = await compareSync(dto.password, user.password);
      if (!checkPass)
        throw new GenericHttpException(LoginFailed, HttpStatus.BAD_REQUEST);
      const token = sign(
        {
          id: user.id,
          email: user.email,
          role: user.role,
        },
        process.env.JWT_SECRET || 'DEVELOPMENT_JWT_SECRET',
        { expiresIn: 60 * 60 }, // 1h
      );
      return token;
    } catch (error) {
      console.log({ error });
      throw new GenericHttpException(
        SystemError,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
