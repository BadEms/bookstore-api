import { User } from '@db/entities/user';
import { DatabaseModule } from '@db/module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtStrategy } from 'src/utils/auth/JwtStrategy';
import { CommonController } from './common.controller';
import { CommonService } from './common.service';

@Module({
  imports: [DatabaseModule, TypeOrmModule.forFeature([User])],
  controllers: [CommonController],
  providers: [CommonService, JwtStrategy],
  exports: [JwtStrategy, CommonService, TypeOrmModule],
})
export class CommonModule {}
