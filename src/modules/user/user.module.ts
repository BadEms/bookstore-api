import { DatabaseModule } from '@db/module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Stock } from '@db/entities/stock';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { Store } from '@db/entities/store';

@Module({
  imports: [DatabaseModule, TypeOrmModule.forFeature([Store, Stock])],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService, TypeOrmModule],
})
export class UserModule {}
