import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Query,
  UseGuards,
} from '@nestjs/common';
import { UserRequired } from 'src/utils/decorator/request';
import { JwtGuard } from 'src/utils/guard/JwtGuard';
import { UserService } from './user.service';
import {
  allStores,
  booksOfStores,
  pagination,
  searchBook,
  searchBooksRequest,
} from './user.dto';
import { plainToClass } from 'class-transformer';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('stores')
  @UseGuards(JwtGuard)
  @UserRequired(true)
  @HttpCode(HttpStatus.OK)
  async getAllStores(@Query() dto: pagination) {
    const response = await this.userService.getAllStores(dto);
    return plainToClass(allStores, response, { excludeExtraneousValues: true });
  }

  @Get('books/:storeId')
  @UseGuards(JwtGuard)
  @UserRequired(true)
  @HttpCode(HttpStatus.OK)
  async getBooksOfStore(
    @Query() dto: pagination,
    @Param('storeId') storeId: number,
  ) {
    const response = await this.userService.getBooksOfStore(dto, storeId);
    return plainToClass(booksOfStores, response, {
      excludeExtraneousValues: false,
      enableImplicitConversion: true,
    });
  }

  @Get('search/books')
  @UseGuards(JwtGuard)
  @UserRequired(true)
  @HttpCode(HttpStatus.OK)
  async searchBook(
    @Query() dto: pagination,
    @Body() books: searchBooksRequest,
  ) {
    const response = await this.userService.searchBook(dto, books);
    return plainToClass(searchBook, response, {
      excludeExtraneousValues: false,
      enableImplicitConversion: true,
    });
  }
}
