import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { Stock } from '@db/entities/stock';
import { pagination, searchBooksRequest } from './user.dto';
import { Store } from '@db/entities/store';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(Store)
    private readonly storeRepository: Repository<Store>,
    @InjectRepository(Stock)
    private readonly stockRepository: Repository<Stock>,
  ) {}

  /**
   * get all bookstores
   * @param dto
   * @returns
   */
  async getAllStores(dto: pagination) {
    try {
      return this.storeRepository.findAndCount({
        order: { name: 'DESC' },
        take: dto.size,
        skip: dto.page,
      });
    } catch (error) {
      console.log({ error });
      throw error;
    }
  }

  /**
   * get books in given store
   * @param dto
   * @param storeId
   * @returns
   */
  async getBooksOfStore(dto: pagination, storeId: number) {
    try {
      return this.stockRepository.findAndCount({
        where: { storeId },
        order: { id: 'DESC' },
        relations: { book: true },
        take: dto.size,
        skip: dto.page,
      });
    } catch (error) {
      console.log({ error });
      throw error;
    }
  }

  /**
   * search book each bookstores
   * @param dto
   * @param data
   * @returns
   */
  async searchBook(dto: pagination, data: searchBooksRequest) {
    try {
      return this.stockRepository.findAndCount({
        where: { bookId: In(data.books) },
        order: { id: 'DESC' },
        relations: { store: true },
        take: dto.size,
        skip: dto.page,
      });
    } catch (error) {
      console.log({ error });
      throw error;
    }
  }
}
