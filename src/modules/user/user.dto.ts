import { Exclude, Expose } from 'class-transformer';
import { IsArray, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';

export class pagination {
  @IsOptional()
  @IsNumber()
  page: number;

  @IsOptional()
  @IsNumber()
  size: number;
}

export class allStores {
  @Expose()
  id: number;

  @Expose()
  name: string;

  @Expose()
  address: string;

  @Exclude()
  createdAt: string;

  @Exclude()
  updatedAt: string;

  @Exclude()
  deletedAt: string;
}

export class books {
  @Exclude()
  createdAt: string;

  @Exclude()
  updatedAt: string;

  @Exclude()
  deletedAt: string;

  @Expose()
  id: number;

  @Expose()
  name: string;
}

export class booksOfStores {
  @Exclude()
  id: number;

  @Exclude()
  storeId: number;

  @Exclude()
  bookId: number;

  @Expose()
  quantity: number;

  @Expose()
  book: books;
}

export class stores {
  @Exclude()
  createdAt: string;

  @Exclude()
  updatedAt: string;

  @Exclude()
  deletedAt: string;

  @Expose()
  id: number;

  @Expose()
  name: string;

  @Expose()
  address: string;
}

export class searchBook {
  @Exclude()
  id: number;

  @Exclude()
  storeId: number;

  @Exclude()
  bookId: number;

  @Expose()
  quantity: number;

  @Expose()
  store: stores;
}

export class searchBooksRequest {
  @IsNotEmpty()
  @IsArray()
  books: number[];
}
