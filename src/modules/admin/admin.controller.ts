import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AdminService } from './admin.service';
import {
  newBook,
  newStore,
  newUser,
  querySetManagerRole,
  requestManageBook,
} from './admin.dto';
import { AdminRequired } from 'src/utils/decorator/request';
import { JwtGuard } from 'src/utils/guard/JwtGuard';
import { UserRole } from '@db/enums/userRole';

@Controller('admin')
export class AdminController {
  constructor(private readonly adminService: AdminService) {}

  @Post('user')
  @UseGuards(JwtGuard)
  @AdminRequired(true)
  @HttpCode(HttpStatus.CREATED)
  async createUser(@Body() dto: newUser) {
    await this.adminService.createUser(dto);
  }

  @Post('set-admin/:id')
  @UseGuards(JwtGuard)
  @AdminRequired(true)
  @HttpCode(HttpStatus.OK)
  async setAdminRole(@Param('id') userId: string) {
    await this.adminService.setRoleToUser(userId, UserRole.ADMIN);
  }

  @Post('set-user/:id')
  @UseGuards(JwtGuard)
  @AdminRequired(true)
  @HttpCode(HttpStatus.OK)
  async setUserRole(@Param('id') userId: string) {
    await this.adminService.setRoleToUser(userId, UserRole.USER);
  }

  @Post('set-manager/:id')
  @UseGuards(JwtGuard)
  @AdminRequired(true)
  @HttpCode(HttpStatus.OK)
  async setManagerRole(
    @Param('id') userId: string,
    @Query() dto: querySetManagerRole,
  ) {
    await this.adminService.setRoleToManager(
      userId,
      UserRole.STORE_MANAGER,
      dto.storeId,
    );
  }

  @Post('store')
  @UseGuards(JwtGuard)
  @AdminRequired(true)
  @HttpCode(HttpStatus.CREATED)
  async createStore(@Body() dto: newStore) {
    dto.name = dto.name.replace(/ /g, '').toLowerCase();
    await this.adminService.createStore(dto);
  }

  @Post('book')
  @UseGuards(JwtGuard)
  @AdminRequired(true)
  @HttpCode(HttpStatus.CREATED)
  async createBook(@Body() dto: newBook) {
    dto.name = dto.name.replace(/ /g, '').toLowerCase();
    await this.adminService.createBook(dto.name);
  }

  @Post('manage/book')
  @UseGuards(JwtGuard)
  @AdminRequired(true)
  @HttpCode(HttpStatus.OK)
  async manageBook(@Body() dto: requestManageBook) {
    return this.adminService.manageBookByAdmin(dto);
  }
}
