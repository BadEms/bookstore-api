export interface IAction {
  bookName: string;
  storeName: string;
  quantity: number;
  message?: string;
}
