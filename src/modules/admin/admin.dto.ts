import {
  IsArray,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsNumberString,
  IsString,
  MinLength,
} from 'class-validator';
import { ProcessType } from 'src/utils/enum';

export class newUser {
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  password: string;
}

export class newStore {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  address: string;
}

export class newBook {
  @IsNotEmpty()
  @IsString()
  name: string;
}

export class manageBook {
  @IsNotEmpty()
  @IsNumber()
  bookId: number;

  @IsNotEmpty()
  @IsNumber()
  storeId: number;

  @IsNotEmpty()
  @IsNumber()
  quantity: number;
}

export class requestManageBook {
  @IsNotEmpty()
  @IsArray()
  books: manageBook[];

  @IsNotEmpty()
  @IsEnum(ProcessType)
  type: string;
}

export class querySetManagerRole {
  @IsNotEmpty()
  @IsNumberString()
  storeId: number;
}
