import { User } from '@db/entities/user';
import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { newStore, newUser, requestManageBook } from './admin.dto';
import { GenericHttpException } from 'src/utils/error';
import { encrypt } from 'src/utils/encryption';
import { UserRole } from '@db/enums/userRole';
import { Store } from '@db/entities/store';
import { Book } from '@db/entities/book';
import { Stock } from '@db/entities/stock';
import { ProcessType } from 'src/utils/enum';
import { IAction } from './admin.interface';
import { NOT_REGISTERED_BOOK, NOT_REGISTERED_STORE } from './admin.constant';
import {
  AdminCannotChange,
  AlreadyManager,
  BookAlreadyExist,
  BookNotFound,
  InsufficienQuantitytError,
  MissingData,
  StoreAlreadyExist,
  StoreNotFound,
  UserAlreadyExist,
  UserNotFound,
} from 'src/utils/error/types';
import { StoreManager } from '@db/entities/storeManager';

@Injectable()
export class AdminService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Store)
    private readonly storeRepository: Repository<Store>,
    @InjectRepository(Book)
    private readonly bookRepository: Repository<Book>,
    @InjectRepository(Stock)
    private readonly stockRepository: Repository<Stock>,
    @InjectRepository(StoreManager)
    private readonly managerRepository: Repository<StoreManager>,
  ) {}

  /**
   * add a new user
   * @param user
   */
  async createUser(user: newUser) {
    try {
      const existUser = await this.userRepository.findOne({
        where: { email: user.email },
      });
      if (existUser)
        throw new GenericHttpException(
          UserAlreadyExist,
          HttpStatus.BAD_REQUEST,
        );
      const newUser = new User();
      newUser.email = user.email;
      newUser.password = await encrypt(user.password);
      newUser.role = UserRole.NONE;
      await this.userRepository.save(newUser);
    } catch (error) {
      console.log({ error });
      throw error;
    }
  }

  /**
   * set user role
   * @param userId
   * @param role
   */
  async setRoleToUser(userId: string, role: string) {
    try {
      const user = await this.userRepository.findOne({ where: { id: userId } });
      if (!user)
        throw new GenericHttpException(UserNotFound, HttpStatus.BAD_REQUEST);
      if (user.role == UserRole.ADMIN)
        throw new GenericHttpException(
          AdminCannotChange,
          HttpStatus.BAD_REQUEST,
        );
      user.role = UserRole[role];
      await this.userRepository.save(user);
    } catch (error) {
      console.log({ error });
      throw error;
    }
  }

  /**
   * set store manager role
   * @param userId
   * @param role
   * @param storeId
   */
  async setRoleToManager(userId: string, role: string, storeId: number) {
    try {
      const user = await this.userRepository.findOne({ where: { id: userId } });
      if (!user)
        throw new GenericHttpException(UserNotFound, HttpStatus.BAD_REQUEST);
      if (user.role == UserRole.ADMIN)
        throw new GenericHttpException(
          AdminCannotChange,
          HttpStatus.BAD_REQUEST,
        );
      if (user.role == UserRole.STORE_MANAGER)
        throw new GenericHttpException(AlreadyManager, HttpStatus.BAD_REQUEST);
      const store = await this.storeRepository.findOne({
        where: { id: storeId },
      });
      if (!store)
        throw new GenericHttpException(StoreNotFound, HttpStatus.BAD_REQUEST);
      const storeManager = new StoreManager();
      storeManager.userId = user.id;
      storeManager.storeId = storeId;
      await this.managerRepository.save(storeManager);
      user.role = UserRole[role];
      await this.userRepository.save(user);
    } catch (error) {
      console.log({ error });
      throw error;
    }
  }

  /**
   * create a new store
   * @param store
   */
  async createStore(store: newStore) {
    try {
      const existStore = await this.storeRepository.findOne({
        where: { name: store.name },
      });
      if (existStore)
        throw new GenericHttpException(
          StoreAlreadyExist,
          HttpStatus.BAD_REQUEST,
        );
      const newStore = new Store();
      newStore.name = store.name;
      newStore.address = store.address;
      await this.storeRepository.save(newStore);
    } catch (error) {
      console.log({ error });
      throw error;
    }
  }

  /**
   * create a new book
   * @param name
   */
  async createBook(name: string) {
    try {
      const existBook = await this.bookRepository.findOne({
        where: { name: name },
      });
      if (existBook)
        throw new GenericHttpException(
          BookAlreadyExist,
          HttpStatus.BAD_REQUEST,
        );
      const newBook = new Book();
      newBook.name = name;
      await this.bookRepository.save(newBook);
    } catch (error) {
      console.log({ error });
      throw error;
    }
  }

  /**
   * manage book by admin
   * @param dto
   * @returns
   */
  async manageBookByAdmin(dto: requestManageBook) {
    try {
      const success: IAction[] = [];
      const failed: IAction[] = [];
      const response = dto.books.map(async (book) => {
        const stock = await this.stockRepository.findOne({
          where: { bookId: book.bookId, storeId: book.storeId },
          relations: { book: true, store: true },
        });
        if (stock) {
          if (
            dto.type == ProcessType.REMOVE &&
            stock.quantity < book.quantity
          ) {
            const failedAction: IAction = {
              bookName: stock.book.name,
              storeName: stock.store.name,
              quantity: stock.quantity,
              message: InsufficienQuantitytError.message,
            };
            failed.push(failedAction);
          } else {
            stock.quantity = stock.quantity - book.quantity;
            await this.stockRepository.save(stock);
            const successAction: IAction = {
              bookName: stock.book.name,
              storeName: stock.store.name,
              quantity: stock.quantity,
            };
            success.push(successAction);
          }
        } else {
          const processBook = await this.bookRepository.findOne({
            where: { id: book.bookId },
          });
          const processStore = await this.storeRepository.findOne({
            where: { id: book.storeId },
          });
          if (!processBook && processStore) {
            const failedAction: IAction = {
              bookName: NOT_REGISTERED_BOOK,
              storeName: processStore.name,
              quantity: 0,
              message: BookNotFound.message,
            };
            failed.push(failedAction);
          } else if (!processStore && processBook) {
            const failedAction: IAction = {
              bookName: processBook.name,
              storeName: NOT_REGISTERED_STORE,
              quantity: 0,
              message: StoreNotFound.message,
            };
            failed.push(failedAction);
          } else if (!processBook && !processStore) {
            const failedAction: IAction = {
              bookName: NOT_REGISTERED_BOOK,
              storeName: NOT_REGISTERED_STORE,
              quantity: 0,
              message: MissingData.message,
            };
            failed.push(failedAction);
          } else if (
            dto.type == ProcessType.REMOVE &&
            processBook &&
            processStore
          ) {
            const failedAction: IAction = {
              bookName: processBook.name,
              storeName: processStore.name,
              quantity: 0,
              message: InsufficienQuantitytError.message,
            };
            failed.push(failedAction);
          } else if (
            dto.type == ProcessType.ADD &&
            processBook &&
            processStore
          ) {
            const newStock = new Stock();
            newStock.bookId = book.bookId;
            newStock.storeId = book.storeId;
            newStock.quantity = book.quantity;
            const successAction: IAction = {
              bookName: processBook.name,
              storeName: processStore.name,
              quantity: book.quantity,
            };
            await this.stockRepository.save(newStock);
            success.push(successAction);
          }
        }
      });
      await Promise.all(response);
      return {
        success,
        failed,
      };
    } catch (error) {
      console.log({ error });
      throw error;
    }
  }
}
