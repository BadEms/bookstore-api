import { User } from '@db/entities/user';
import { DatabaseModule } from '@db/module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';
import { Store } from '@db/entities/store';
import { Book } from '@db/entities/book';
import { Stock } from '@db/entities/stock';
import { StoreManager } from '@db/entities/storeManager';

@Module({
  imports: [
    DatabaseModule,
    TypeOrmModule.forFeature([User, Store, Book, Stock, StoreManager]),
  ],
  controllers: [AdminController],
  providers: [AdminService],
  exports: [AdminService, TypeOrmModule],
})
export class AdminModule {}
